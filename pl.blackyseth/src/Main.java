
public class Main {
    public static void main(String args[]){

        Bank bank = new Bank();
        Account acc1 = new Account("123456789");
        Account acc2 = new Account("987654321");

        Income income1 = new Income(bank,2);
        Income income2 = new Income(bank,7);
        Transfer transfer1 = new Transfer(bank,acc2,3);
        Transfer transfer2 = new Transfer(bank,acc1,23);

        acc1.doOperation(income1);
        acc1.doOperation(transfer1);
        acc2.doOperation(transfer2);
        acc2.doOperation(income2);
        acc1.getAccountInfo();
        acc2.getAccountInfo();
    }
}
