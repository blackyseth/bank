import java.util.ArrayList;
import java.util.List;

public class History {
    private final List<HistoryLog> historyLogStack = new ArrayList<HistoryLog>();

    public void addHistoryLogStack(String title, OperationType type) {
        historyLogStack.add(new HistoryLog(title,type));
    }

    public void getHistoryLogStack() {
        for(HistoryLog log: historyLogStack){
            System.out.println(log.getDateOfOperation()+" "+log.getTitle()+" "+log.getOperationType());
        }
    }

}
