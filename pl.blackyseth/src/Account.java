

public class Account {

    private String number;
    private double amountOfMoney = 1000;
    private final History history = new History();

    public Account(){
        setNumber("no name");
    }

    public Account(String number){
        setNumber(number);
    }

    public void add(double amount){
        this.amountOfMoney = amountOfMoney + amount;
    }

    public void subtract(double amount){
        this.amountOfMoney = amountOfMoney - amount;
    }

    public void addHistory(String title, OperationType type){
        history.addHistoryLogStack(title,type);
    }

    public void getAccountInfo(){
        System.out.println(number);
        System.out.println("This acc have: "+amountOfMoney);
        System.out.println("History of transactions: ");
        getHistory();
    }

    private void getHistory(){
        history.getHistoryLogStack();
    }

    public void doOperation(Operation op){
        op.execute(this);
    }

    private void setNumber(String number) {
        this.number = number;
    }

}
