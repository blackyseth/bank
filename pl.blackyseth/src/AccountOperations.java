
public interface AccountOperations {
    void income(Account account, double amount);
    void transfer(Account from, Account to, double amount);
}
