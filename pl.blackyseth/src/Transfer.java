
public class Transfer implements Operation{

    private final AccountOperations operations;

    private final Account to;
    private final double amount;

    public Transfer(AccountOperations operations,Account to, double amount){
        this.to = to;
        this.amount = amount;
        this.operations = operations;
    }

    @Override
    public void execute(Account account) {
        operations.transfer(account,to,amount);
    }
}
