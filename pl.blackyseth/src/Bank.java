
public class Bank implements AccountOperations{

    public void income(Account account, double amount){
        account.add(amount);
        account.addHistory("Income of "+amount,OperationType.income);
    }

    public void transfer(Account from, Account to, double amount){
        from.subtract(amount);
        from.addHistory("Outcome of " + amount, OperationType.outcome);
        to.add(amount);
        to.addHistory("Income of "+amount,OperationType.income);
    }
}
