import java.util.Date;

public class HistoryLog {
    private final String title;
    private final OperationType operationType;

    public HistoryLog(String title, OperationType type){
        getDateOfOperation();
        this.title = title;
        this.operationType = type;
    }

    public String getDateOfOperation(){
        Date dateOfOperation = new Date();
        return dateOfOperation.toString();
    }

    public String getTitle(){
        return title;
    }

    public String getOperationType(){
        return operationType.toString();
    }
}
