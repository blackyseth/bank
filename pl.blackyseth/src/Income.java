
public class Income implements Operation{

    private final AccountOperations operations;
    private final double amount;

    public Income(AccountOperations operations, double amount){
        this.operations = operations;
        this.amount = amount;
    }

    @Override
    public void execute(Account account) {
        operations.income(account, amount);
    }

}
